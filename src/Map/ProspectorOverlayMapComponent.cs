﻿using System.Linq;
using System.Text;
using Vintagestory.API.Client;
using Vintagestory.API.MathTools;
using Vintagestory.GameContent;

namespace ProspectorInfo.Map
{
    internal class ProspectorOverlayMapComponent : MapComponent
    {
        public readonly int ChunkX;
        public readonly int ChunkZ;
        private readonly string _message;
        private readonly int _chunksize;

        private LoadedTexture texture;
        private Vec3d worldPos = new Vec3d();
        private Vec2f viewPos = new Vec2f();
        private bool renderTexture;

        public ProspectorOverlayMapComponent(ICoreClientAPI clientApi, int chunkX, int chunkZ, string message, bool renderTexture = true) : base(clientApi)
        {
            ChunkX = chunkX;
            ChunkZ = chunkZ;
            _message = message;
            _chunksize = clientApi.World.BlockAccessor.ChunkSize;

            this.renderTexture = renderTexture;
            if (!renderTexture)
                return;
            this.texture = new LoadedTexture(clientApi, 0, _chunksize, _chunksize);
            capi.Render.LoadOrUpdateTextureFromRgba(Enumerable.Repeat(ColorUtil.ToRgba(128, 150, 128, 150), _chunksize * _chunksize).ToArray(), false, 0, ref texture);
            capi.Render.BindTexture2d(texture.TextureId);

            this.worldPos = new Vec3d(chunkX * _chunksize, 0, chunkZ * _chunksize);
        }

        public override void OnMouseMove(MouseEvent args, GuiElementMap mapElem, StringBuilder hoverText)
        {
            var worldPos = new Vec3d();
            float mouseX = (float)(args.X - mapElem.Bounds.renderX);
            float mouseY = (float)(args.Y - mapElem.Bounds.renderY);

            mapElem.TranslateViewPosToWorldPos(new Vec2f(mouseX, mouseY), ref worldPos);

            var chunkX = (int)(worldPos.X / _chunksize);
            var chunkZ = (int)(worldPos.Z / _chunksize);
            if (chunkX == ChunkX && chunkZ == ChunkZ)
            {
                hoverText.AppendLine($"\n{_message}");
            }
        }

        public override void Render(GuiElementMap map, float dt)
        {
            if (!this.renderTexture)
                return;

            map.TranslateWorldPosToViewPos(this.worldPos, ref this.viewPos);

            base.capi.Render.Render2DTexture(
                this.texture.TextureId,
                (int)(map.Bounds.renderX + viewPos.X),
                (int)(map.Bounds.renderY + viewPos.Y),
                (int)(this.texture.Width * map.ZoomLevel),
                (int)(this.texture.Height * map.ZoomLevel),
                50);
        }
    }
}